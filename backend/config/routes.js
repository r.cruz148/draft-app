const express = require('express')

module.exports = function(server) {
	// API Routes
	const router = express.Router()
	server.use('/api', router)

	const playerService = require('../api/player/playerService')
	playerService.register(router, '/players')

	const clubService = require('../api/club/clubService')
	clubService.register(router, '/clubs')
}