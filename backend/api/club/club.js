const restful = require('node-restful')
const mongoose = restful.mongoose
const Player = require('../player/player')

const clubSchema = new mongoose.Schema({
	club: { type: String, required: true, unique: true  },
	players: [{ type: mongoose.Schema.Types.ObjectId }]
})

//db.clubs.aggregate([{ $lookup: { from: "players", localField: "players", foreignField: "_id", as: "embeddedData" } }]).pretty()

module.exports = restful.model('Club', clubSchema)