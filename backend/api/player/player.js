const restful = require('node-restful')
const mongoose = restful.mongoose

const playerSchema = new mongoose.Schema({
	name: { type: String, required: true },
	position: { type: String, required: true, uppercase: true,
		enum: [ "GK", "CB", "LB", "LWB", "RB", "RWB", "RF", "RW", "RM", "LF", "LW", "LM", "CAM", "CM", "CDM", "ST", "CF" ]},
	rating: { type: Number, min: 1, max: 99, required: true }
})

module.exports = restful.model('Player', playerSchema)