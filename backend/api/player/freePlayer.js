const restful = require('node-restful')
const mongoose = restful.mongoose
const Player = require('./player')

const freePlayerSchema = new mongoose.Schema({
	players: [{ type: mongoose.Schema.Types.ObjectId }]
})

//db.clubs.aggregate([{ $lookup: { from: "players", localField: "players", foreignField: "_id", as: "embeddedData" } }]).pretty()

module.exports = restful.model('FreePlayer', freePlayerSchema)